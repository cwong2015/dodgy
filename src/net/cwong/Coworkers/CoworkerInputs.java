package net.cwong.Coworkers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Scanner;

public class CoworkerInputs implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Scanner scan, scan2, scan3;
	private String filePath;
	private FileOutputStream fos;
	private File[] files;
	private int optionChoice;
	private String input;
	private String fileFolder;

	public CoworkerInputs(String fileFolder) {

		this.fileFolder = fileFolder;
	}

	public void runProgram() {
		// initialize the scanner
		scan = new Scanner(System.in);

		System.out.println("Enter a name to save. Type and enter \"read\" to view the list of people already added.");
		System.out.println("Enter \"select\" to view options and select a person.");
		// store user input for name or accesses list
		input = scan.nextLine();

		if (input.equalsIgnoreCase("read")) {
			files = new File(fileFolder).listFiles();
			printOptions(files);
			addMore();
		} else if (input.equalsIgnoreCase("select")) {
			files = new File(fileFolder).listFiles();
			printOptions(files);
			System.out.println("\nNow enter your choice.");
			optionChoice = scan.nextInt();
			choiceReader(files, optionChoice);
		} else {
			nameDescScanners(scan, input);
		}

	}

	private void addMore() {
		scan = new Scanner(System.in);
		System.out.println("Add more? Enter \"yes\" to continue.");
		input = scan.nextLine();

		if (input.equalsIgnoreCase("yes")) {
			runProgram();

		}
	}

	private void nameDescScanners(Scanner scan, String input) {

		// create instance of CoworkerMod and set the name data
		CoworkerMod c = new CoworkerMod(input, null);

		System.out.println("Now enter their description.");

		// store user input for description
		String desc = scan.nextLine();

		// call .setDesc to set our description
		c.setDesc(desc);

		filePath = fileFolder + input + ".txt";

		serializeCoworkerFileDesc(desc, filePath, fos);

		System.out.println("Name and description saved\n");

		continueProgram();
	}

	public static void serializeCoworkerFileDesc(String desc, String filePath, FileOutputStream fos) {
		try {
			fos = new FileOutputStream(filePath);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(fos, "utf-8"))) {
			writer.write(desc);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void continueProgram() {
		CoworkerInputs a = new CoworkerInputs("C:\\Users\\C\\Documents\\Coworkers\\");

		System.out.print("Enter yes to continue, no to finish, read to select options");

		scan2 = new Scanner(System.in);
		String response = scan2.nextLine();

		if (response.equalsIgnoreCase("yes")) {
			a.runProgram();
		}

		else if (response.equalsIgnoreCase("no")) {

			System.out.println("\nProgram has terminated.");
		} else if (response.equalsIgnoreCase("read")) {
			files = new File(fileFolder).listFiles();
			System.out.println("Please select one of the options below and enter it:");
			printOptions(files);
			scan3 = new Scanner(System.in);
			optionChoice = scan3.nextInt();
			choiceReader(files, optionChoice);

		}

	}

	private void printOptions(File[] files) {
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile()) {
				System.out.println(i + 1 + ". " + (files[i].getName().substring(0, files[i].getName().length() - 4)));
			}

		}

	}

	private void choiceReader(File[] files, int optionChoice) {
		BufferedReader reader = null;

		try {
			File file = files[optionChoice - 1];
			reader = new BufferedReader(new FileReader(file));

			String line;
			if ((line = reader.readLine()) != null) {
				System.out.println(line);
				addMore();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
}