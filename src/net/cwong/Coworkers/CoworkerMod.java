package net.cwong.Coworkers;
public class CoworkerMod implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private String name;
	private String description;
	
	public CoworkerMod(String myName, String myDes){
		name = myName;
		description = myDes;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDes(){
		return description;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setDesc(String desc){
		this.description = desc;
	}
}
