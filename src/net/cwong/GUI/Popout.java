package net.cwong.GUI;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import net.cwong.Coworkers.CoworkerInputs;

public class Popout extends Application {
	private Button add;
	private TextField field;
	private Text text;
	private Scene scene;
	private String name;
	private String fileFolder;
	private String filePath;
	private FileOutputStream fos;
	private String description;
	private ComboBox<String> choices;

	@Override
	public void start(Stage primaryStage) throws Exception {
		add = new Button("Add");
		GridPane gp = new GridPane();
		field = new TextField();
		text = new Text();

		gp.setHgap(10);
		gp.setVgap(10);
		gp.setAlignment(Pos.CENTER);

		add.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				serializeData();

			}
		});

		text.setText("Enter a description for " + name + ".");

		gp.add(text, 0, 1);
		gp.add(field, 0, 2);
		gp.add(add, 0, 3);

		scene = new Scene(gp, 200, 250);

		primaryStage.setScene(scene);
		primaryStage.show();

	}

	//saves the name of the person as the file name and their description inside of the file as a .txt
	protected void serializeData() {
		filePath = fileFolder + "\\" + name + ".txt";
		try {
			fos = new FileOutputStream(filePath);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (field != null) {
			description = field.getText();
			System.out.println(
					"Name is " + "\"" + name + "\"" + " and description is " + "\"" + description + "\"" + ".");
			CoworkerInputs.serializeCoworkerFileDesc(description, filePath, fos);
			
		}
	}

	public void setName(String name) {
		this.name = name;

	}

	public void setFilePath(String fileFolder) {
		this.fileFolder = fileFolder;
	}

	public void setChoices(ComboBox<String> choices) {
		this.choices = choices;
		
	}

}
