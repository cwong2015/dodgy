package net.cwong.GUI;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class MainView extends Application {
	//instance variables 
	private Scene scene;
	private TextField tf;
	private ComboBox<String> choices;
	private GridPane grid;
	private HBox hbtn;
	private Button add, select, browse;
	private DirectoryChooser dc;
	private File selectFolder;
	private File[] files;
	private Text text;
	private Text text2;
	private Alert alert;
	private Alert alert2;
	private Alert descriptionAlert;
	private Alert noPath;
	private String fileFolder;
	private String name;
	private ArrayList<String> arrayList;
	private HBox hbtn2;
	private String choiceName;
	private File fileReadFrom;
	private Text selectDesc;
	private Button refresh;

	@Override
	public void start(Stage stage) throws Exception {

		stage.setTitle("Dodgy Reader");
		
		choices = new ComboBox<String>();
		choices.setEditable(true);
		
		add = new Button();
		add.setText("Add");
		add.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				addButtonCases();

			}

		});
		
		//button to refresh combo box options after a new person is added to the directory system 
		refresh = new Button();
		refresh.setText("Refresh Menu");
		refresh.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent arg0) {
				refreshCombo();
				
			}
			
		});

		//button for the client to click after selecting their option from the combo box
		select = new Button();
		select.setText("Select");
		select.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				selectOption();

			}

		});

		//button leads to the DirectoryChooser, allowing clients to select a folder 
		browse = new Button();
		browse.setText("Browse Folders");
		browse.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				openDirectoryChooser();

			}

		});

		//HBox is used to place components together without overlapping in a location
		hbtn = new HBox(10);
		hbtn.setAlignment(Pos.BOTTOM_LEFT);
		hbtn.getChildren().addAll(add);

		hbtn2 = new HBox(10);
		hbtn2.setAlignment(Pos.CENTER);
		hbtn2.getChildren().addAll(choices, select);

		tf = new TextField();

		text = new Text("Enter a name below to add to your files.");

		text2 = new Text("Select a folder.");

		selectDesc = new Text("Select a person.");

		alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Information Dialog");

		alert2 = new Alert(AlertType.ERROR);
		alert2.setTitle("Error Dialog");

		noPath = new Alert(AlertType.ERROR);
		noPath.setTitle("Error Dialog");

		descriptionAlert = new Alert(AlertType.INFORMATION);
		descriptionAlert.setTitle("Dodgy?");

		grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.add(tf, 0, 34);
		grid.add(hbtn, 0, 35);
		grid.add(browse, 1, 20);
		grid.add(hbtn2, 0, 20);
		grid.add(text, 0, 33);
		grid.add(text2, 1, 19);
		grid.add(selectDesc, 0, 19);
		grid.add(refresh, 0, 21);

		scene = new Scene(grid, 500, 500);

		stage.setScene(scene);
		stage.show();
	}

	protected void refreshCombo() {
		constructCombo(choices);
		
	}

	//method that reads the option chose from the combo box
	//displays the description of the person chosen
	protected void selectOption() {
		BufferedReader reader = null;

		choiceName = choices.getValue();
		int count = arrayList.indexOf(choiceName);
		fileReadFrom = files[count];

		try {
			reader = new BufferedReader(new FileReader(fileReadFrom));

			String line;
			if ((line = reader.readLine()) != null) {
				descriptionAlert.setHeaderText(
						(files[count].getName().substring(0, files[count].getName().length() - 4)) + " is ... ");
				descriptionAlert.setContentText("..." + line + "!");
				descriptionAlert.showAndWait();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	//some small error catching when it comes to the add button
	protected void addButtonCases() {
		if (tf.getText().trim().equals("")) {
			alert2.setTitle("Error");
			alert2.setContentText("Field cannot be empty!");
			alert2.showAndWait();

		} else if (fileFolder == null) {
			noPath.setContentText("Choose a directory first by clicking the Browse button!");
			noPath.show();
		} else {
			name = tf.getText();
			Stage stage = new Stage();
			Popout po = new Popout();
			po.setName(name);
			po.setFilePath(fileFolder);
			po.setChoices(choices);
			try {
				po.start(stage);
				constructCombo(choices);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	//constructed the names into the combo box by cutting out the names of the files without the .txt part
	//used the for loop to update the number of each person added to the list (i + ". " + [code to get the name])
	private void constructCombo(ComboBox<String> choices) {
		arrayList = new ArrayList<String>();
		files = new File(fileFolder).listFiles();
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile()) {
				arrayList.add(i + 1 + ". " + (files[i].getName().substring(0, files[i].getName().length() - 4)));
			}

		}
		
		//this effectively clears the items from a previously chosen directory and adds in the new ones
		choices.getItems().clear();
		choices.getItems().addAll(arrayList);
	}

	public static void main(String[] args) throws Exception {
		launch(args);
	}

	//opens the file explorer using DirectoryChooser
	private void openDirectoryChooser() {
		dc = new DirectoryChooser();
		selectFolder = dc.showDialog(null);

		if (selectFolder != null) {

			alert.setContentText("You have selected the folder \"" + selectFolder.getName() + "\".\n"
					+ "Your files for this program will now be saved into that folder.\n"
					+ "You may also view the data by selecting from the menu.");
			alert.showAndWait();
			fileFolder = selectFolder.getAbsolutePath();
			constructCombo(choices);

		} else {
			System.out.println("File selection cancelled.");
		}

	}

}
